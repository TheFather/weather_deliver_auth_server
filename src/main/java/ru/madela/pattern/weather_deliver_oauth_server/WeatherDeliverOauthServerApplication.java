package ru.madela.pattern.weather_deliver_oauth_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeatherDeliverOauthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherDeliverOauthServerApplication.class, args);
    }
}
