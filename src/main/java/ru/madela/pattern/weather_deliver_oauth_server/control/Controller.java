package ru.madela.pattern.weather_deliver_oauth_server.control;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.Client;
import ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model.IClientDao;

import java.util.UUID;

@RestController
@RequestMapping("/client")
public class Controller {
    private final IClientDao clientDaoService;

    public Controller(IClientDao clientDaoService) {
        this.clientDaoService = clientDaoService;
    }

    @GetMapping("/{id}")
    public Client getClient(@PathVariable(name = "id") UUID id) {
        return clientDaoService.getById(id);
    }
}
