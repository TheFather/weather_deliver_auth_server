package ru.madela.pattern.weather_deliver_oauth_server.sql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "client_table")
public class Client {
    @Id
    private UUID id;

    @Column(name = "secret", nullable = false, unique = true)
    private String secret;

    @Column(name = "redirect", nullable = false)
    private String registeredRedirectUri;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "client_scope",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "scope_id")
    )
    private Set<Scope> scopeSet;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "client_grant_type",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "grant_type_id")
    )
    private Set<GrantType> grantTypeSet;
}
