package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

import ru.madela.pattern.weather_deliver_oauth_server.sql.entity.Client;

public interface IClientDao extends IMonoSelectable<Client> {
}
