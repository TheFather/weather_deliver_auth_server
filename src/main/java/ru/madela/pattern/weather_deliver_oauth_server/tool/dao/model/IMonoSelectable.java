package ru.madela.pattern.weather_deliver_oauth_server.tool.dao.model;

import java.util.UUID;

public interface IMonoSelectable<T> {
    T getById(UUID id);
}
