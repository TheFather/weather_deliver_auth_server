package ru.madela.pattern.weather_deliver_oauth_server.util.dto;

import lombok.*;

import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@With
@Builder
public class ClientDto {
    private UUID id;
    private String secret;
    private String registeredRedirectUri;
    private Set<ScopeDto> scopeSet;
    private Set<GrantTypeDto> grantTypeSet;
}
